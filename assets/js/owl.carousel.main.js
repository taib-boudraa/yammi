$(function () {
    $("#carousel1").owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      center: true,
      autoplay: true,
      timeout: 3000,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 2,
        },
        1000: {
          items: 4,
        },
      },
    });
});